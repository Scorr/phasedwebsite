"use strict";

//=============================================//
// Global Objects
//=============================================//

// OpenGL context
var gl;
// Renderer handler
var renderer;
// System updater
var last_time;
var mouse_x = 0;
var mouse_y = 0;

// Scene objects
var scene_sprite0;

//=============================================//
// Program entry
//=============================================//

function start()
{
	// Create the renderer
	renderer = new CRenderer();

    // Set start time for delta time
    var d = new Date();
    last_time = d.getTime();

    // Create the scene
    scene_sprite0 = new CSpriteSplitLoad ("./sprites/pc_fall.png", 32, 32, 7);
    scene_sprite0.sprite.animationSpeed = 15.0;
    scene_sprite0.sprite.scale = 4;

    // Set up the update
    setInterval( update, 15 );
}

//=============================================//
// Scene tickers
//=============================================//

function update()
{
    // Calculate delta
    var d = new Date();
    var current_time = d.getTime();
    var delta_time = (current_time - last_time) / 1000.0;

    // Update all scene object
    scene_sprite0.sprite.x = gl.canvas.width / 2  - (mouse_x - gl.canvas.width / 2) * 0.2;
    scene_sprite0.sprite.y = gl.canvas.height / 2 - (mouse_y - gl.canvas.height / 2) * 0.2;
    scene_sprite0.Tick(delta_time);

    // Save previous update time
    last_time = current_time;
}
function render()
{
    scene_sprite0.Render(gl);
}


//=============================================//
// System CB
//=============================================//

document.onmousemove = ev_handleMouseMove;
function ev_handleMouseMove(event)
{
    var dot, eventDoc, doc, body, pageX, pageY;

    event = event || window.event; // IE-ism

    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX == null && event.clientX != null)
    {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
        event.pageY = event.clientY +
          (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
          (doc && doc.clientTop  || body && body.clientTop  || 0);
    }

    // Use event.pageX / event.pageY here
    mouse_x = event.pageX;
    mouse_y = event.pageY;
}

//=============================================//
// Renderer class
//=============================================//

function init2d( canvas )
{
	gl = null;
	// Try to get either webgl or experimental
	try {
		gl = canvas.getContext("2d");
	}
	catch(e) {}

	// No context? Fuck.
	if (!gl)
	{
		alert("Unable to initialize WebGL. Your browser is weak!");
		gl = null;
	}

	// Return newly found context
	return gl;
}
function refitCanvas ()
{
    // For mobile: get the display pixel ratio
    var realToCSSPixels = window.devicePixelRatio || 1;

    // Get the canvas from the WebGL context
    var canvas = gl.canvas;

    // Lookup the size the browser is displaying the canvas.
    var displayWidth  = Math.floor(canvas.clientWidth  * realToCSSPixels);
    var displayHeight = Math.floor(canvas.clientHeight * realToCSSPixels);

    // Check if the canvas is not the same size.
    if (canvas.width  != displayWidth || canvas.height != displayHeight)
    {
        // Make the canvas the same size
        canvas.width  = displayWidth;
        canvas.height = displayHeight;
    }
}

function CRenderer ()
{
	// Grab the canvas element
	var canvas = document.getElementById("glcanvas");

	// Create the context
	gl = init2d(canvas);
	// Check the new context
	if (gl)
	{
		setInterval( this.RenderScene, 15 );
	}
}
CRenderer.prototype =
{
	// Main scene renderloop
	RenderScene :function ()
	{
        refitCanvas();

        gl.beginPath();
        gl.rect(0,0, gl.canvas.width, gl.canvas.height);
        gl.fillStyle = "rgba(0,0,0,1)";
        gl.fill();

        // Set up rendering options
        gl.imageSmoothingEnabled = false;
        gl.mozImageSmoothingEnabled = false;
        gl.webkitImageSmoothingEnabled = false;
        gl.msImageSmoothingEnabled = false;

        // Render the scene
        render();
	}
}

//=============================================//
// Sprite class
//=============================================//

function CSprite ( frameWidth, frameHeight, frameCount )
{
    this.frameWidth = frameWidth;
    this.frameHeight = frameHeight;
    this.frameCount = frameCount;
}
CSprite.prototype =
{
    image :null,
    x :0,
    y :0,
    scale :1.0,
    frameIndex :0,
    frameWidth :0,
    frameHeight :0,
    frameCount :0,
    centered :false,

    animationIndex :0.0,
    animationSpeed :0.0,

    // Load sprite
    LoadFromURI :function ( sprite_uri )
    {
        this.image = new Image();
        this.image.src = sprite_uri;
    },

    // Sprite update
    Tick :function ( deltaTime )
    {
        this.animationIndex += deltaTime * this.animationSpeed;
        this.frameIndex = Math.floor(this.animationIndex) % this.frameCount;
    },
    // Render sprite
    Render :function ( ctx )
    {
        // Only render if have valid properties
        if ( this.image != null )
        {
            var apparent_width = this.frameWidth * this.scale;
            var apparent_height = this.frameHeight * this.scale;
            ctx.drawImage(
                this.image,
                this.frameIndex * this.frameWidth, 0,
                this.frameWidth, this.frameHeight,
                this.x - (this.centered ? 0:(apparent_width/2)), this.y - (this.centered ? 0:(apparent_height/2)),
                apparent_width, apparent_height
            );
        }
    }
}

//=============================================//
// Sprite effect fade-in class
//=============================================//

function CSpriteSplitLoad ( sprite_uri, frameWidth, frameHeight, frameCount )
{
    this.sprite = new CSprite( frameWidth, frameHeight, frameCount );
    this.image = new Image();
    this.image.src = sprite_uri;

    // Create temporary buffers for colorizing the left and right abberation
    this.canvas_r = document.createElement("canvas");
    this.canvas_r.width = frameWidth;
    this.canvas_r.height = frameHeight;
    this.context_r = this.canvas_r.getContext("2d");
    this.canvas_b = document.createElement("canvas");
    this.canvas_b.width = frameWidth;
    this.canvas_b.height = frameHeight;
    this.context_b = this.canvas_b.getContext("2d");
}
CSpriteSplitLoad.prototype =
{
    // Sprite with animation data
    sprite :null,

    // Image to load
    image :null,

    // Abberation buffers
    canvas_r :null,
    context_r :null,
    canvas_b :null,
    context_b :null,

    // Current split stage (-1 off, 0 to 1 is full to none)
    split_val :-1.0,

    // Update
    Tick :function ( deltaTime )
    {
        if ( this.image.complete )
        {
            this.split_val = Math.min(1.0, this.split_val + deltaTime);
            this.sprite.Tick(deltaTime);
        }
        else
        {
            this.split_val = -1.0;
        }
    },
    // Render sprite
    Render :function ( ctx )
    {
        if ( this.split_val > 0.0 && this.sprite != null )
        {
            if ( this.split_val < 0.99 )
            {   // Perform the split:
                var offset = (1.0 - this.split_val) * 10.0;

                // Set red bg
                this.context_r.globalAlpha = this.split_val;
                this.context_r.globalCompositeOperation = "source-over";
                //this.context_r.beginPath();
                this.context_r.fillStyle = "rgba(255,64,64,1)";
                this.context_r.fillRect(0, 0, this.canvas_r.width, this.canvas_r.height);
                // Mask against the image
                this.context_r.globalCompositeOperation = "destination-in";
                this.context_r.drawImage(
                    this.image,
                    this.sprite.frameIndex * this.sprite.frameWidth, 0,
                    this.sprite.frameWidth, this.sprite.frameHeight,
                    -offset,0, this.sprite.frameWidth, this.sprite.frameHeight
                );
                // Multiply the image
                this.context_r.globalCompositeOperation = "multiply";
                this.context_r.drawImage(
                    this.image,
                    this.sprite.frameIndex * this.sprite.frameWidth, 0,
                    this.sprite.frameWidth, this.sprite.frameHeight,
                    -offset,0, this.sprite.frameWidth, this.sprite.frameHeight
                );

                // Set blue bg
                this.context_b.globalAlpha = this.split_val;
                this.context_b.globalCompositeOperation = "source-over";
                //this.context_r.beginPath();
                this.context_b.fillStyle = "rgba(0,191,191,1)";
                this.context_b.fillRect(0, 0, this.canvas_b.width, this.canvas_b.height);
                // Mask against the image
                this.context_b.globalCompositeOperation = "destination-in";
                this.context_b.drawImage(
                    this.image,
                    this.sprite.frameIndex * this.sprite.frameWidth, 0,
                    this.sprite.frameWidth, this.sprite.frameHeight,
                    +offset,0, this.sprite.frameWidth, this.sprite.frameHeight
                );
                // Multiply the image
                this.context_b.globalCompositeOperation = "multiply";
                this.context_b.drawImage(
                    this.image,
                    this.sprite.frameIndex * this.sprite.frameWidth, 0,
                    this.sprite.frameWidth, this.sprite.frameHeight,
                    +offset,0, this.sprite.frameWidth, this.sprite.frameHeight
                );
                // Now we add the other
                this.context_b.globalCompositeOperation = "lighter";
                this.context_b.drawImage(
                    this.canvas_r,
                    0,0, this.sprite.frameWidth, this.sprite.frameHeight,
                    0,0, this.sprite.frameWidth, this.sprite.frameHeight
                );

                this.sprite.image = this.canvas_b;
                this.sprite.frameIndex = 0;
            }
            else
            {   // If done, don't do the expensive split effect
                this.sprite.image = this.image;
            }
            this.sprite.Render( ctx );
        }
    }
}
